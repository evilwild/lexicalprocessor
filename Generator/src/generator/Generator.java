package generator;

import syntax_analyzer.syntax_tree.SyntaxTree;
import syntax_analyzer.syntax_tree.SyntaxTreeNode;

public class Generator {

  SyntaxTree tree;
  StringBuilder structuredText;

  public Generator(SyntaxTree tree) {
    this.tree = tree;
    this.structuredText = new StringBuilder();
  }

  public String generateText() {
    traverseTree(tree.getRoot(), 0);
    return this.structuredText.toString();
  }

  private void traverseTree(SyntaxTreeNode root, int indentSize) {
    if (root.getChilds().isEmpty()) {
      int k = 0;
      if (root.getNodeSymbol() == '+' || root.getNodeSymbol() == '*')
        k = 2;
      for (int i = 0; i < indentSize + k; i++) {
        this.structuredText.append(" ");
      }
      String nodeContent = (root.getNodeSymbol() == '1') ? String.valueOf(Integer.parseInt(root.getNodeContent(), 2)) : root.getNodeContent();
      this.structuredText.append(nodeContent);
      this.structuredText.append("\n");
    } else {
      for (SyntaxTreeNode child : root.getChilds()) {
        traverseTree(child, indentSize+2);
      }
    }

  }

//  private void traverseTree(SyntaxTreeNode root, TreeItem<Character> rootItem) {
//    for (SyntaxTreeNode child : root.getChilds() ) {
//      TreeItem<Character> newItem = getTreeItemFromNode(child);
//      rootItem.getChildren().add(newItem);
//      traverseTree(child, newItem);
//    }
//  }
}

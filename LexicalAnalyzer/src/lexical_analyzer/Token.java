package lexical_analyzer;

public enum Token {
  FirstWord,
  SecondWord,
  Commentary,
  PlusSign,
  AsteriskSign,
  Epsilon,
}

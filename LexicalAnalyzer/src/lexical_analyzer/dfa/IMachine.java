package lexical_analyzer.dfa;

import lexical_analyzer.exceptions.IllegalTransition;

public interface IMachine {
	void switchState(char ch) throws IllegalTransition;
	boolean canStop();
}

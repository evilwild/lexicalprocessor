package lexical_analyzer.dfa;

public interface ITransition {
	IState toState();
	boolean accept(char ch);
}

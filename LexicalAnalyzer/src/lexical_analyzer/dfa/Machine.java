package lexical_analyzer.dfa;

import lexical_analyzer.exceptions.IllegalTransition;

public class Machine implements IMachine {

  private IState initialState;
  private IState currentState;

  public Machine(IState startState) {
    this.initialState = startState;
    this.currentState = startState;
  }

  @Override
  public void switchState(char ch) throws IllegalTransition {
    this.currentState = this.currentState.makeTransition(ch);
  }

  @Override
  public boolean canStop() {
    return this.currentState.isFinal();
  }

  public void resetDFA() {
    this.currentState = this.initialState;
  }
}

package lexical_analyzer.dfa;

import lexical_analyzer.Token;

public final class MachineFactory {
	public static Machine getMachine(Token tokenType) {		
		if (tokenType == Token.FirstWord) {
			State A = new State();
			State B = new State();
			State C = new State();
			State D = new State();
			State EFin = new State(true);
			State F = new State();
			State G = new State();
			A.addTransition(new Transition("1", B));
			B.addTransition(new Transition("0", C));
			C.addTransition(new Transition("0", A));
			B.addTransition(new Transition("1", D));
			D.addTransition(new Transition("0", EFin));
			EFin.addTransition(new Transition("1", F));
			F.addTransition(new Transition("0", G));
			G.addTransition(new Transition("0", EFin));
			return new Machine(A);
		} else if (tokenType == Token.SecondWord) {
			State A = new State();
			State B = new State(true);
			State C = new State(true);
			A.addTransition(new Transition("a", B));
			A.addTransition(new Transition("b", C));
			A.addTransition(new Transition("c", B));
			A.addTransition(new Transition("d", B));
			B.addTransition(new Transition("a", B));
			B.addTransition(new Transition("b", B));
			B.addTransition(new Transition("c", B));
			B.addTransition(new Transition("d", B));
			C.addTransition(new Transition("a", C));
			C.addTransition(new Transition("b", C));
			C.addTransition(new Transition("c", C));
			return new Machine(A);
		} else if (tokenType == Token.Commentary) {
			State A = new State();
			State B = new State();
			State C = new State();
			State Fin = new State(true);
			A.addTransition(new Transition("/", B));
			B.addTransition(new Transition("/", C));
			C.addTransition(new Transition("\n", Fin));
			C.addTransition(new Transition("\0", Fin));
			C.addTransition(new Transition("*", C));
			return new Machine(A);
		} else if (tokenType == Token.PlusSign) {
			State A = new State(true);
			A.addTransition(new Transition("+", A));
			return new Machine(A);
		} else if (tokenType == Token.AsteriskSign) {
			State A = new State(true);
			A.addTransition(new Transition("*", A));
			return new Machine(A);
		} else if (tokenType == Token.Epsilon) {
			State A = new State(true);
			A.addTransition(new Transition("", A));
			return new Machine(A);
		}
		return null;
	}
}

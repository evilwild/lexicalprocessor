package lexical_analyzer.dfa;

public class Transition implements ITransition {
	
	private String rule;
	private State nextState;
	
	public Transition(String rule, State nextState) {
		this.rule = rule;
		this.nextState = nextState;
	}
	
	@Override
	public IState toState() {
		return this.nextState;
	}

	@Override
	public boolean accept(char ch) {
		return this.rule.equalsIgnoreCase(String.valueOf(ch));
	}

}

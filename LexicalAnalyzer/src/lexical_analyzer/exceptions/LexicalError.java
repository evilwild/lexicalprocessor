package lexical_analyzer.exceptions;

public class LexicalError extends Exception {
  public LexicalError(String msg) {
    super("Lexical error on " + msg);
  }
}

package syntax_analyzer;

import lexical_analyzer.Scanner;
import lexical_analyzer.exceptions.LexicalError;
import syntax_analyzer.syntax_tree.SyntaxTree;

public interface IParser {
  public static final Scanner scanner = new Scanner();

  SyntaxTree parse() throws SyntaxError, LexicalError, ContextError;

  void setInput(String input);
}

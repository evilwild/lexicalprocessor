package syntax_analyzer;

public class SyntaxError extends Exception {
  public SyntaxError(String msg) {
    super("Syntax error on " + msg);
  }
}

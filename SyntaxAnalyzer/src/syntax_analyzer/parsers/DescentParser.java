package syntax_analyzer.parsers;
import lexical_analyzer.Token;
import lexical_analyzer.exceptions.LexicalError;
import syntax_analyzer.ContextError;
import syntax_analyzer.IParser;
import syntax_analyzer.SyntaxError;
import syntax_analyzer.syntax_tree.SyntaxTree;
import syntax_analyzer.syntax_tree.SyntaxTreeNode;

import java.util.HashSet;

public class DescentParser implements IParser {

  // S -> + S S | A
  // A -> * S S | B
  // B -> <2> | <1>

  Token currentToken;
  SyntaxTree syntaxTree;

  private HashSet<String> S(SyntaxTreeNode S, HashSet<String> ids) throws LexicalError, SyntaxError, ContextError {
    System.out.println("DescentParser.S");
    switch (currentToken) {
      case PlusSign:
        match(Token.PlusSign);
        S.addChildNode('+', "+");
        SyntaxTreeNode S1 = S.addChildNode('S', scanner.getTokenValue());
        HashSet<String> ids_s1 = S(S1, ids);
        SyntaxTreeNode S2 = S.addChildNode('S', scanner.getTokenValue());
        HashSet<String> ids_s2 = S(S2, ids);
        ids_s1.addAll(ids_s2);
        return ids_s1;
      default:
        SyntaxTreeNode A = S.addChildNode('A', scanner.getTokenValue());
        HashSet<String> token = A(A, ids);
        ids.addAll(token);
        return ids;
    }
  }
  
  private HashSet<String> A(SyntaxTreeNode A, HashSet<String> ids) throws LexicalError, SyntaxError, ContextError {
    System.out.println("DescentParser.A");
    System.out.println(currentToken);
    switch(currentToken) {
      case AsteriskSign:
        match(Token.AsteriskSign);
        A.addChildNode('*', "*");
        SyntaxTreeNode S1 = A.addChildNode('S', scanner.getTokenValue());
        HashSet<String> ids_s1 = S(S1, ids);
        SyntaxTreeNode S2 = A.addChildNode('S', scanner.getTokenValue());
        HashSet<String> ids_s2 = S(S2, ids);
        ids_s1.addAll(ids_s2);
        return ids_s1;
      default:
        SyntaxTreeNode B = A.addChildNode('B', scanner.getTokenValue());
        HashSet<String> token = B(B, ids);
        return token;
    }
  }
  
  private HashSet<String> B(SyntaxTreeNode B, HashSet<String> ids) throws LexicalError, SyntaxError, ContextError {
    System.out.println("DescentParser.B");
    System.out.println(currentToken);
    if (currentToken.equals(Token.FirstWord) || currentToken.equals(Token.SecondWord)) {
      String tokenValue = null;
      if (currentToken.equals(Token.SecondWord)){
         tokenValue = IParser.scanner.getTokenValue();
        if (ids.contains(tokenValue))
          throw new ContextError(IParser.scanner.getCurrentTextPosition());
      }
      char nodeSymbol = (currentToken.equals(Token.FirstWord)) ? '1' : '2';
      B.addChildNode(nodeSymbol, scanner.getTokenValue());
      currentToken = IParser.scanner.readNextToken();
      HashSet<String> _tokenValue = new HashSet<String>();
      if (tokenValue != null)
        _tokenValue.add(tokenValue);
      return _tokenValue;
    } else {
      throw new SyntaxError(IParser.scanner.getCurrentTextPosition());
    }
  }

  boolean match(Token token) throws SyntaxError, LexicalError {
    System.out.println("DescentParser.match");
    System.out.println(currentToken);
    if (token.equals(currentToken)) {
      currentToken = IParser.scanner.readNextToken();
      return true;
    }
    throw new SyntaxError(IParser.scanner.getCurrentTextPosition());
  }

  @Override
  public SyntaxTree parse() throws LexicalError, SyntaxError, ContextError {
    System.out.println("DescentParser.parse start");
      syntaxTree = new SyntaxTree();
      syntaxTree.setRoot('S');
      SyntaxTreeNode root = syntaxTree.getRoot();
      currentToken = IParser.scanner.readNextToken();
      HashSet<String> ids = new HashSet<String>();
      S(root, ids);
      match(Token.Epsilon);
      System.out.println("DescentParser.parse end");
      return syntaxTree;
  }

  @Override
  public void setInput(String input) {
    IParser.scanner.resetScanner(input);
  }
}

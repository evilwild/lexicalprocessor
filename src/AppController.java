import generator.Generator;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import lexical_analyzer.exceptions.LexicalError;
import syntax_analyzer.ContextError;
import syntax_analyzer.SyntaxAnalyzer;
import syntax_analyzer.SyntaxAnalyzerMethod;
import syntax_analyzer.SyntaxError;
import syntax_analyzer.syntax_tree.SyntaxTree;
import syntax_analyzer.syntax_tree.SyntaxTreeNode;

import java.net.URL;
import java.util.ResourceBundle;

public class AppController implements Initializable {
  SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(SyntaxAnalyzerMethod.DescentParser);
  public Button button;
  public TextArea input;
  public Label status;
  public TreeView treeview;
  public TextArea structuredTextArea;
  private Border errorBorder;

  public AppController() {
    errorBorder = new Border(new BorderStroke(Color.RED,
        BorderStrokeStyle.SOLID,
        CornerRadii.EMPTY,
        new BorderWidths(5)));
  }

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    button.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        resetStatusLabel();
        button.setDisable(true);
        String inputText = getInputText();
        if (inputText.length() < 1) {
          status.setText("Текст отсутствует");
          input.setBorder(errorBorder);
        } else {
          try {
            input.setBorder(null);
            SyntaxTree syntaxTree = syntaxAnalyzer.analyze(inputText);
            Generator generator = new Generator(syntaxTree);
            String structuredText = generator.generateText();
            structuredTextArea.setText(structuredText);
            TreeItem<Character> rootItem = new TreeItem<Character>(syntaxTree.getRoot().getNodeSymbol());
            treeview.setRoot(rootItem);
            traverseTree(syntaxTree.getRoot(), treeview.getRoot());
          } catch (LexicalError | SyntaxError | ContextError error) {
            input.setBorder(errorBorder);
            treeview.setRoot(null);
            structuredTextArea.setText(null);
            status.setText(error.getMessage());
          }
        }
        button.setDisable(false);
      }
    });
  }

  private TreeItem<Character> getTreeItemFromNode(SyntaxTreeNode syntaxTreeNode) {
    return new TreeItem<Character>(syntaxTreeNode.getNodeSymbol());
  }

  private String getInputText() {
    return this.input.getText();
  }

  private void resetStatusLabel() {
    this.status.setText(null);
  }

  private void traverseTree(SyntaxTreeNode root, TreeItem<Character> rootItem) {
    for (SyntaxTreeNode child : root.getChilds() ) {
      TreeItem<Character> newItem = getTreeItemFromNode(child);
      rootItem.getChildren().add(newItem);
      traverseTree(child, newItem);
    }
  }
}

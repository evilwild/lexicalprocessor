import lexical_analyzer.exceptions.InvalidToken;
import org.junit.jupiter.api.Test;
import syntax_analyzer.SyntaxAnalyzer;
import syntax_analyzer.SyntaxAnalyzerMethod;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MainTest {

  SyntaxAnalyzer sa = new SyntaxAnalyzer(SyntaxAnalyzerMethod.DescentParser);

  @Test
  void shouldFail() {
    assertThrows(InvalidToken.class, () -> sa.analyze("-"));
  }

  String [] args = {
      "* a * 110 a",  // 0
      "+ a * a 110",  // 1
      "* * a 110 110",// 2
      "* * a a 110",  // 3
      "+ a a",        // 4
      "+ + 110 a a",  // 5
      "+ a + + b 110 c",//6
      "* 110 + a a",  // 7
      "* a a",        // 8
      "* a 110",      // 9
  };

  @Test
  void shouldPass0() { assertAll(() -> sa.analyze(args[0])); }
  @Test
  void shouldPass1() { assertAll(() -> sa.analyze(args[1])); }
  @Test
  void shouldPass2() { assertAll(() -> sa.analyze(args[2])); }
  @Test
  void shouldPass3() { assertAll(() -> sa.analyze(args[3])); }
  @Test
  void shouldPass4() { assertAll(() -> sa.analyze(args[4])); }
  @Test
  void shouldPass5() { assertAll(() -> sa.analyze(args[5])); }
  @Test
  void shouldPass6() { assertAll(() -> sa.analyze(args[6])); }
  @Test
  void shouldPass7() { assertAll(() -> sa.analyze(args[7])); }
  @Test
  void shouldPass8() { assertAll(() -> sa.analyze(args[8])); }
  @Test
  void shouldPass9() { assertAll(() -> sa.analyze(args[9])); }
}